---
layout: handbook-page-toc
title: "Cadence"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Overview

Everything in a company happens in a certain cadence.
The period of each cadence differs.
The timescale between periods are about 4x, varying from 3x to 5x.
Below are the cadences we have at GitLab:

1. Day
1. Week (5 workdays)
1. Month (4.3 weeks)
1. Quarter (3 months)
1. Year (4 quarters)
1. Strategy (3 years)
1. Vision (10 years, 3.3x)
1. Mission (30 years, 3x)

## Year at a Glance

```mermaid
gantt
    title Year Overview
    dateFormat  YYYY-MM-DD

    section Fiscal Year at a Glance
    FY starts                   :2020-02-01, 1d
    End of Q1                   :2020-04-30, 1d
    End of Q2                   :2020-07-31, 1d
    End of Q3                   :2020-10-31, 1d
    End of Q4                   :2021-01-31, 1d

    section Major Events
    SKO                         :2020-02-10, 5d

    section E Group Offsite
    February                    :2020-02-24, 4d
    May                         :2020-05-18, 4d
    August (date estimated)     :2020-08-17, 4d
    November (date estimated)   :2020-11-16, 4d

    section Board Meeting
    Prep led by CoS             :2020-02-14, 21d
    March                       :2020-03-05, 1d
    Prep led by CoS             :2020-05-14, 21d
    June (date estimated)       :2020-06-05, 1d
    Prep led by CoS             :2020-08-14, 21d
    September (date estimated)  :2020-09-05, 1d
    Prep led by CoS             :2020-11-14, 21d
    December (date estimated)   :2020-12-05, 1d

    section Monthly Releases
    12.7                        :2020-01-22, 1d
    12.8                        :2020-02-22, 1d
    12.9                        :2020-03-22, 1d
    12.10                       :2020-04-22, 1d
    12.11                       :2020-05-22, 1d
    13.0                        :2020-06-22, 1d
    13.1                        :2020-07-22, 1d
    13.2                        :2020-08-22, 1d
    13.3                        :2020-09-22, 1d
    13.4                        :2020-10-22, 1d
    13.5                        :2020-11-22, 1d
    13.6                        :2020-12-22, 1d

    section OKR
    OKR prep                    :2020-03-30, 31d
    next FQ starts              :2020-05-01, 1d
    OKR prep                    :2020-06-30, 31d
    next FQ starts              :2020-08-01, 1d
    OKR prep                    :2020-08-30, 31d
    next FQ starts              :2020-10-01, 1d

    section Sales Key Dates
    Q2 QBRs                     :2020-05-04, 4d
    Q3 QBRs                     :2020-08-04, 4d
    Q4 QBRs                     :2020-11-04, 4d
    Q1 QBRs                     :2021-02-04, 4d

    section Compensation Review Cycle
    Audit FY 2021 Annual Compensation Review :2020-02-01
    Canada Benefits Implementation :2020-02-01
    Job Family Alignment         :2020-03-01
    Compensation Philosophy Training :2020-04-01
    Benefits Survey Released FY21 :2020-06-01
    Benefits Survey Results FY21 Analyzed :2020-07-01
    Catch-up Compensation Review Manager Review :2020-08-01
    Catch-up Compensation Review  :2020-09-01
    Compensation Training         :2020-10-01
    Performance Factor Review            :2020-11-01
    Annual Comp Review Inputs Evaluated/Proposed to Compensation Group for FY22 :2020-12-01


    section People Dates
    DIB Survey                      :2020-02-27, 14d
    Code of Conduct Acknowledgement :2020-03-01, 30d
    Engagement Survey (date estimated) :2020-04-27, 14d
    360s                            :2020-04-30, 30d
    Engagement Survey(estimated)    :2020-10-26, 14d
```

## Day

- [Group conversation](/handbook/group-conversations/)

## Week

- [1-1 cadence with reports](/handbook/leadership/1-1/)
- [Measure growth of early startup, 10% WoW](https://about.gitlab.com/blog/2020/05/05/wow-rule/)
- [E-Group Weekly](/handbook/e-group-weekly/)

## Month

- [Key Meetings](/handbook/key-meetings/)
- [Release](/releases/)
- [Retrospective](/handbook/communication/#kickoffs)
- [Most KPIs](/handbook/business-ops/data-team/metrics/)

## Quarter

- [OKRs](/company/okrs/)
- [Board meeting](/handbook/board-meetings/#board-meeting-process)
- Sales targets (in [Clari](/handbook/business-ops/tech-stack/#clari))
- [E-group offsite](/company/offsite/)

## Year

- [4 quarter rolling forecast](/handbook/finance/financial-planning-and-analysis/#quarterly-forecast-rolling-four-quarters)
- [Annual plan](/handbook/finance/financial-planning-and-analysis/#planning-process--gitlab)
- Most of [Direction](/direction/)

## Strategy

3 years

- Most of [strategy](/company/strategy/)
- Some of [Direction](/direction/)
- [Option vesting](/handbook/stock-options/#vesting) after cliff of 1 year is passed
- Average retention of team members is around 3 years, with reduced [turnover](/handbook/people-group/people-operations-metrics/#team-member-turnover) (<16%). See [actual reports](/handbook/people-group/people-operations-metrics/#reporting)

## Vision

10 years

- [Product vision](/direction/#vision)
- Time needed for category creation
- [Investor fund time limit](https://www.strictlybusinesslawblog.com/2017/06/29/the-life-cycle-of-a-private-equity-or-venture-capital-fund/)
- Commitment of DZ

## Mission

30 years

- Time to realize [our mission](/company/mission-and-vision/#mission)
- [BHAG](/company/mission-and-vision/#big-hairy-audacious-goal)
- [Lifespan of the average company](https://www.bbc.com/news/business-16611040), 10 years to get into the S&P500, then 15 in it, and 5 of decline for a total of 30
- [Lifespan of Amazon](https://www.forbes.com/sites/richardkestenbaum/2018/11/16/amazon-is-not-too-big-to-fail-bezos/#65fba0621626) "Amazon is not too big to fail...In fact, I predict one day Amazon will fail. Amazon will go bankrupt. If you look at large companies, their lifespans tend to be 30-plus years, not a hundred-plus years."
- [Generation is also 30 years](https://www.ncbi.nlm.nih.gov/pubmed/10677323)
