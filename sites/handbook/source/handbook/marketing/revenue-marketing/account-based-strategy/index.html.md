---
layout: handbook-page-toc
title: "Account Based Strategy"
---
## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Account Based Strategy
The account based strategy team sits in [Revenue Marketing](/handbook/marketing/revenue-marketing/) and is responsible for the following:
- Developing (in partnership with sales and marketing) and maintaining our [ideal customer profile (ICP)](/handbook/marketing/revenue-marketing/account-based-strategy/#ideal-customer-profile)
- Maintenance of GitLab's [focus account lists](/handbook/marketing/revenue-marketing/account-based-strategy/#focus-account-lists)
- [Account based marketing (ABM)](/handbook/marketing/revenue-marketing/account-based-strategy/#account-based-marketing-abm) campaigns and tactics
- [Demandbase](/handbook/marketing/revenue-marketing/account-based-strategy/#demandbase)
    - platform management
    - user training
    - account scoring
    - account based engagement
    - campaign orchestration

## Ideal Customer Profile
Our ideal customer profiles are the description of our "perfect" customer company (not individual or end user).  Each profile takes into consideration a variety of firmographic, environmental and behavioral factors to determine our ideal customers.  The ICP's are, in turn, used in turn to develop our focus account lists. Visit the [ICP handbook page](/handbook/marketing/revenue-marketing/account-based-strategy/ideal-customer-profile/) for more information.

## Focus account lists
Our `Focus Account Lists` are a list of account that match our ICP and are heavily skewed towards landing [first order logos](/handbook/sales/#first-order-customers) for GitLab and fit our ideal customer profile.  The `Large` account list is called the GL4300 and the `Mid-Market` account list is called the MM4000.  We do not currently have a focus account list for `SMB`.  The lists are developed using the current data sources we have at our disposal and are refreshed quarterly at the beginning of each quarter. These account lists make up our 1:many account based marketing motion.  For more information and to see the current list criteria, please review the [Focus account list handbook page](/handbook/marketing/revenue-marketing/account-based-strategy/focus-account-lists).

## Account Based Marketing (ABM)
Account-based marketing is a focused B2B marketing approach in which marketing and sales teams work together to target key or "best-fit" accounts. These accounts are marketed to directly in a 1:1 or 1:few campaign.  At GitLab, we focus on the top net new and growth `Large` accounts globally which are nominated by sales each quarter.  For more information on our account based marketing strategy and tactics please review our [ABM handbook page](/handbook/marketing/revenue-marketing/account-based-strategy/account-based-marketing/)

## Demandbase
Demandbase is an account based experience platform and the engine behind our account based strategy campaigns, the development of our `focus account lists` and is our primary display advertising platform.  Check out the [Demandbase handbook page](/handbook/marketing/revenue-marketing/account-based-strategy/demandbase/) for more information on the platform and how different teams utilize it.

## Project and priority management
The account based strategy teams maintains several boards to help manage our projects, priorities and workflow.  The main board is the [Account Based Strategy](https://gitlab.com/groups/gitlab-com/-/boards/2297659) board and has the following projects and labels:

### Projects
- Account Based Strategy- anything that relates to our ideal customer profile, focus account lists, and anything the team is working on that crosses over multiple departments i.e. working with Sales Ops on account data updates
- Account Based Marketing- specific to our account based marketing strategy and campaigns
- Demandbase- anything related to Demandbase platform management, intent data, and orchestration (DB campaign requests should be open in the project that covers the audience the campaign is launching to)

### Labels
- Account Based Strategy- anything the team is working on that crosses over multiple projects i.e. working with Sales Ops on account data updates
- Account Based Marketing- specific to our account based marketing strategy and campaigns
- Ideal Customer Profile- covers cross functional work on maintaing our ICP
- Focus Account Lists- for updates and management of our focus account lists, as well as campaigns targeting these lists
- Demandbase- anything related to Demandbase platform management, intent data, and orchestration (DB campaign requests should be open in the project that covers the audience the campaign is launching to)
