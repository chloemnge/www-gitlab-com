---
layout: markdown_page
title: "Tyler Williams' README"
description: "Learn more about working with Tyler Williams"
---

## Tyler Williams' README

**Tyler Williams, Website Full Stack Developer** This page is intended to help others understand what it might be like to work with me, especially people who haven’t worked with me before. 

It’s also a well-intentioned effort at building some trust by being intentionally vulnerable, and to share my ideas of a good working relationship to reduce the anxiety of people who might be on my team.

Please feel free to contribute to this page by opening a merge request. 

## Related pages

- [GitLab Handle](https://gitlab.com/tywilliams)
- [LinkedIn](https://www.linkedin.com/in/tylerwilliamsct/)
- [My Website](https://ogdenstudios.xyz/)
- [Team Page](https://about.gitlab.com/company/team/#tywilliams)

## About me

I was interviewed by [Noah Gibbs](https://codefol.io/) for his podcast, [Computer Science, Just the Useful Bits](https://anchor.fm/just-the-useful-bits/). If you want to hear about my academic/professional journey in software, and get a sense for how I approach the world, [my episode](https://anchor.fm/just-the-useful-bits/episodes/With-Tyler-Williams-Hands-On-Teaching-Between-Students-Formal-and-Informal-Teaching-eitt1f) is a great deep dive. 

### Tidbits

- I am quick to laugh and slow to anger. 
- I am a super-fan of [the Mountain Goats](https://www.mountain-goats.com/). 
- I am originally from Connecticut. My family still lives in and maintains the [Treat-Debisschop Homestead](https://patch.com/connecticut/oxford-ct/historical-society-updating-house-book), built in 1796.
- These days, I live in the middle of a prairie dog open space reserve, just north of Denver.
- I worked at [my college radio station](https://whus.org). My operator handle was `DJ Cool Software`. Schedule a coffee chat and ask me about the origins of the name - it's a fun story, a touch more involved than you might initially guess.
- Before the COVID-19 pandemic, I played bass in a three-piece punk band, Throwing Things.
- In my spare time, I volunteer as a mentor at [Emergent Works](https://www.emergentworks.org/).
- I am fiercely loyal to my friends. I believe my greatest calling in life is to bring people together. 

### GitLab Values that Resonate with Me

Three of GitLab's values connect with me on a deep and personal level: 

**[Diversity, Inclusion & Belonging](https://about.gitlab.com/handbook/values/#diversity-inclusion):** I owe a lot of my success to the existing biases in tech hiring. I empathize with folks who don't have those same advantages, and I want to make the software industry (and world writ large) more equitable and inclusive. I hope to meaningfully demonstrate to anyone who works with me that I take diversity, inclusion, and belonging seriously. 

**[Iteration](https://about.gitlab.com/handbook/values/#iteration):** years ago, a mentor introduced me to the [Getting Things Done (GTD)](https://gettingthingsdone.com/) methodology. It produced transformative results in my academic, professional, and personal life. I think GTD is aligned with the sub-values described in GitLab's iteration value, like [don't wait](https://about.gitlab.com/handbook/values/#dont-wait), [set a due date](https://about.gitlab.com/handbook/values/#iteration), and [reduce cycle time](https://about.gitlab.com/handbook/values/#reduce-cycle-time). I'm excited to practice this kind of iterative work with a team that values it so highly and learn even more about being an interative contributor.

**[Transparency](https://about.gitlab.com/handbook/values/#transparency):** if you skim my [blog](https://ogdenstudios.xyz/blog), you'll find a mix of long form polished articles and small bits of note-taking. I was highly influenced by [Learn In Public](https://www.swyx.io/learn-in-public/). GitLab's approach is in alignment with the things I've gained from learning in public. I believe GitLab's commitment to transparency is also a positive feedback loop that contributes to things like improving diversity, inclusion & belonging. 

## How you can help me

I learn and process best with written or verbal materials, rather than visuals. I prefer a README doc to an infographic. I prefer a podcast to a livestream. I'm confident that being [handbook first](https://about.gitlab.com/handbook/handbook-usage/#why-handbook-first) will be a good fit for my work style.

I jump to solution-oriented thinking quickly, sometimes before having enough context and requirements. At its best, I hope this presents as a healthy [bias for action](https://about.gitlab.com/handbook/values/#bias-for-action). But at its worst, I may be struggling with the idea that [it's impossible to know everything](https://about.gitlab.com/handbook/values/#its-impossible-to-know-everything). By jumping to solutions, I am trying to prove my own ability to my collaborators. Help me by being clear about whether we are iterating on solutions, scoping out challenges, or something else. 

I get excited by new projects and ideas, sometimes to the detriment of existing priorities. Iteration is attractive to me in that way, but please help me by reminding me to [always iterate deliberately](https://about.gitlab.com/handbook/values/#always-iterate-deliberately).

## My working style

I love to experiment with things hands on. When we begin collaborating, I will embrace GitLab's [permission to play](https://about.gitlab.com/handbook/values/#permission-to-play) and quickly spin up demos and prototypes. 

I'm a verbal processor. Sometimes the best way for me to get un-stuck is to spitball and brainstorm in a video call. As I work on my [asynchronous communication](https://about.gitlab.com/handbook/communication/#asynchronous-communication) skills, I will experiment with [writing things down](https://about.gitlab.com/handbook/values/#write-things-down) to achieve the similar results without requiring synchronous communication.

### Myers-Briggs Personality type: 

[**Turbulent Protagonist** (ENFJ-T)](https://www.16personalities.com/enfj-personality)

**Individual traits:**

Mind: **94% Extraverted**

Energy: **88% Intuitive**

Nature: **93% Feeling**

Tactics: **53% Judging**

Identity: **88% Turbulent**

## What I assume about others

I usually assume and operate as though we have identical priorities or backlogs. Help me by reminding me what's most important to you, so I can focus on [efficiency for the right group](https://about.gitlab.com/handbook/values/#efficiency-for-the-right-group).

I assume you want me to _solve your problem right here and now_. Help me by being clear about what we're collaborating on. Remind me that [people are not their work](https://about.gitlab.com/handbook/values/#people-are-not-their-work), and we can collaborate without me handing you a perfect solution right then and there. 

I assume you're looking for me to be an expert or provide the right answer on the first try. Help me by reminding me that it's OK to be uncertain at first. It's worth repeating that I may need some help internalizing the sub-value that [it's impossible to know everything](https://about.gitlab.com/handbook/values/#its-impossible-to-know-everything). 

I assume the choices we make now will be hard to reverse. Most teams I've worked with don't iterate quickly and rarely have time to undo decisions. Help me keep [two-way doors](https://about.gitlab.com/handbook/values/#make-two-way-door-decisions) at the forefront of the discussion.

## What I want to earn

- I want to earn respect as a subject matter expert. I want people to consider me to be highly capable.
- I want to earn the trust of my team. I want my team to consider me to be highly reliable.
- I want to earn a good rapport. I want my team to look forward to collaborating with me.

## Communicating with me

It bears repeating that I am not a morning person. I'm usually warmed up by 10:00 MT. I take pride in being punctual, so I won't miss early meetings, but I will not be tip-top until I've had a second cup of coffee or so. 

I have a young, energetic dog. He's extremely good at telling time. At the top of each hour, he asks to go on a walk. I take it as an opportunity to get up from my work and stretch my legs. So around every hour (or after a long block of meetings), I tend to be away from my computer for a short break.

I follow my calendar to a fault. If an engagement doesn't exist on my calendar, I am likely to miss it. I promise to be diligent about documenting appointments and other calendar-worthy information. 