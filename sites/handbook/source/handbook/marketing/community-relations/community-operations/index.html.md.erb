---
layout: handbook-page-toc
title: "Community Operations"
---

{::options parse_block_html="true" /}

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Our mission

GitLab's Community Operations Program is responsible for developing and maintaining the infrastructure and resources to support the Community Relations team and the GitLab community at large. In this, we help support GitLab's greater mission of 'everyone can contribute' by encouraging the wider GitLab community through process and opportunity.  

### Partnership

The mission of this program is to act as a partner to all Program Managers in the Community Relations team to define, implement and refine KPIs/PIs to measure and report the success and effectiveness of our community programs. We work together with the Community Relations team’s Program Managers to produce regular, engaging content to highlight their programs and attract new contributors.

[Community Relations Handbook](https://about.gitlab.com/handbook/marketing/community-relations/)

The Community Operations function also works closely with the Marketing Operations, and Data and Analytics teams.

### Tooling

Community Operations defines and maintains the tool stack required to measure and interact with the wider GitLab community.
The Community Program Manager acts as the DRI for the Community Relations team’s webpages on about.gitlab.com. This program also supports the Open Source and Education teams by processing program applications and renewals. Ultimately, we are working towards a process to fully automate this.

#### Community Operations Tool Stack

These are the tools the Community Relations team is the DRI for:

<div class="responsive-table">
  <table>
    <thead>
      <tr>
        <th>
          Tool
        </th>
        <th>
          Description
        </th>
        <th>
          How we use it
        </th>
        <th>
          Technical owner
        </th>
      </tr>
    </thead>
    <tbody>
    <%# Generate the toolstack table from the data/tech_stack.yml file: -%>
    <%# See the file's key-value pair schema at: https://about.gitlab.com/handbook/business-ops/tech-stack-applications/#what-data-lives-in-the-tech-stack -%>
    <% data.tech_stack.each do |tool| -%>
      <% if (tool.group_owner == 'Community Relations') or (['Zapier', ].include?(tool.title)) -%>
        <tr>
          <td>
            <%= tool.title %>
          </td>
          <td>
            <%# Filter out null entries before rendering markdown -%>
            <% if tool.description -%>
              <%= kramdown(tool.description) %>
            <% end %>
          </td>
          <td>
            <%# Filter out null entries before rendering markdown -%>
            <% if tool.handbook_link -%>
              <%= kramdown(tool.handbook_link) %>
            <% end %>
          </td>
          <td>
            <%= tool.technical_owner %>
          </td>
        </tr>
      <% end %>
    <% end %>
    </tbody>
  </table>
</div>

##### Community Operations Tool Stack (deprecated)

<i class="fas fa-hand-point-right" aria-hidden="true" style="color: rgb(138, 109, 59)
;"></i> This overview is currently being deprecated as we attempt to migrate from
the manually-generated list to one automatically-generated from the `data/tech_stack.yml` file.
Please use the [automatically-generated table instead](#community-operations-tool-stack)
{: .alert .alert-warning}

<details>
<summary markdown="span">Click to show deprecated table</summary>

These are the tools the Community Relations team is the DRI for:

| Tool Name | Description | How We Use
|-------------|-------------|-----------|
| Bitergia | [Bitergia](gitlab.biterg.io/) is the platform we use to measure and track metrics related to contributing code and documentation to GitLab | [How we use Bitergia](/handbook/marketing/community-relations/code-contributor-program/#bitergia-dashboard) |
| Crowdin | [Crowdin](https://translate.gitlab.com/) is the platform for the wider community to collaboratively contribute translations for GitLab | [How we use Crowdin](https://docs.gitlab.com/ce/development/i18n/translation.html) |
| Discourse | [Discourse](https://www.discourse.org) is the platform on which the [GitLab forum](https://forum.gitlab.com) is run. | [How we use Discourse](/handbook/marketing/community-relations/community-operations/workflows/forum/#administration)|
| Disqus  | [Disqus](https://disqus.com) is the commenting on blog.gitlab.com and docs.gitlab.com | [How we use Disqus](/handbook/marketing/community-relations/community-operations/tools/disqus) |
| Gitter | [Gitter](https://gitter.im/gitlab/home) is the instant messaging platform the GitLab community communicates on (in addition to GitLab.com itself) | We currently have a `gitlab` community with 3 channels: `gitlab` (general conversation, questions about GitLab), `contributors` (chat and support about contributing to GitLab) and `heroes` (GitLab Heroes chat) |
| KeyHole | [KeyHole](https://keyhole.io) is the tool we use to collect Twitter impressions and YouTube views | [How we use KeyHole](https://about.gitlab.com/handbook/marketing/community-relations/developer-evangelism/metrics/#metrics-collections) |
| Meetup | [Meetup.com](https://www.meetup.com/pro/gitlab/) is the platform we use and offer to our community to organize meetups | [How we use Meetup.com](/handbook/marketing/community-relations/evangelist-program/#meetups) |
| SameRoom | [SameRoom](https://sameroom.io/) is the platform we use to bridge public Gitter channels with the private GitLab Slack instance | SameRoom enables bidirectional communication from Slack to Gitter and viceversa. Currently it is enabled for the `gitlab/contributors` Gitter channel and the reciprocal `#gitter-contributors-room` on Slack. Messages sent from Slack are forwarded to Gitter using the `gitter-badger` account, but otherwise they show the display name of the Slack user who sent the message.|
| SheerId | [SheerId](https://www.sheerid.com/) is the platform we use to automatically qualify applications to our community programs | |
| Zapier |  [Zapier](https://zapier.com) is an automation tool used to identify mentions and to route them into Zendesk as tickets, and also to Slack in some cases | [How we use Zapier](/handbook/marketing/community-relations/community-operations/tools/zapier) |
| Zendesk | [Zendesk](https://www.zendesk.com/support/) is the tool Community Ops, EDU & OSS work their program cases and applications  | [How we use Zendesk](/handbook/marketing/community-relations/community-operations/zendesk/)|

</details>

##### Tools pending addition to tech stack

The Community Relations team is also the DRI for these tools which are [pending addition to the tech stack](/handbook/business-ops/tech-stack-applications/#add-new-system-to-the-tech-stack):

| Tool | Description | How we use it |
|---|---|---|
| Gitter | [Gitter](https://gitter.im/gitlab/home) is the instant messaging platform the GitLab community communicates on (in addition to GitLab.com itself) | We currently have a `gitlab` community with 3 channels: `gitlab` (general conversation, questions about GitLab), `contributors` (chat and support about contributing to GitLab) and `heroes` (GitLab Heroes chat) |
| KeyHole | [KeyHole](https://keyhole.io) is the tool we use to collect Twitter impressions and YouTube views | [How we use KeyHole](https://about.gitlab.com/handbook/marketing/community-relations/developer-evangelism/metrics/#metrics-collections) |
| SameRoom | [SameRoom](https://sameroom.io/) is the platform we use to bridge public Gitter channels with the private GitLab Slack instance | SameRoom enables bidirectional communication from Slack to Gitter and viceversa. Currently it is enabled for the `gitlab/contributors` Gitter channel and the reciprocal `#gitter-contributors-room` on Slack. Messages sent from Slack are forwarded to Gitter using the `gitter-badger` account, but otherwise they show the display name of the Slack user who sent the message.|
| SheerId | [SheerId](https://www.sheerid.com/) is the platform we use to automatically qualify applications to our community programs | |

##### Other tools we rely on

These are the tools that are essential to some Community programs, but the Community Relations team are not the DRI for:

| Tool Name | Description | How We Use
|-------------|-------------|-----------|
| Customer Portal | [CustomersDot](/handbook/engineering/development/fulfillment/architecture/#customersdot) - Web portal where customers can manage their subscriptions and account information. | To help troubleshoot issues with community program applications |
| License Portal | [LicenseApp](/handbook/engineering/development/fulfillment/architecture/#licensedot) - Web portal to generate and manage GitLab licenses. | To create and manage licenses for community program applications and for [GitLab EE contributors](/handbook/marketing/community-relations/code-contributor-program/#contributing-to-the-gitlab-enterprise-edition-ee) |
| Marketo | [Marketo](/handbook/marketing/marketing-operations/marketo/) | Powers each intake form for our ([Education](/solutions/education/), [Open Source](/solutions/open-source/), and [Startups](/solutions/startups/)) programs. It is an integration which inserts the application record into Salesforce. |
| Printfection |[Printfection](https://www.printfection.com/) is our swag management platform  | [How we use Printfection](/handbook/marketing/corporate-marketing/merchandise-handling) |
| Salesforce  | [Salesforce](https://www.salesforce.com) is our [CRM](https://en.wikipedia.org/wiki/Customer_relationship_management) | We use Salesforce (SFDC) to [support the Education, Open Source and Startup Programs](/handbook/marketing/community-relations/community-operations/community-program-applications)   |

### GitLab's Community

The Community Operations Program curates and maintains documentation for any team member to productively engage with the wider community. When necessary, we engage with specialists within GitLab to provide responses and listen to our community’s feedback on [The GitLab forum](https://forum.gitlab.com), the [GitLab blog](https://about.gitlab.com/blog/) and Hackernews.

The [Community Operations Manager](https://about.gitlab.com/handbook/marketing/community-relations/#who-we-are) reports to the [Director of the Community Relations Team](https://about.gitlab.com/job-families/marketing/director-of-community-relations/).

## Community Operations Work

You can find the Community Operations Program peppered throughout the Community Relations handbook, processes, and projects.

In order to loop in Community Operations on GitLab.com, please use the `community-ops` label.

Everything labeled `community-ops` is organized on the [Community Operations Issue Board](https://gitlab.com/groups/gitlab-com/marketing/community-relations/-/boards/2062229?assignee_username=LindsayOlson&label_name[]=Community%20Ops).

### How to Use the Community Operations Issue Board

| Column Name | Description |
|-------------|-------------|
| Open        | All open issues with the `community-ops-` label   |
| Todo        | Issues with a due date or high priority   |
| Doing       | Issues that are currently in-flight (these have action items for the Community Operations Manager)   |
| Closed      | Issues that have been closed or completed   |

### How to Use the community-ops Label in GitLab

Please use the `community-ops` label only when there is an action item needed from the Community Operations Manager.

If you think an issue or MR is a "nice to know" for the Community Operations Manager, feel free to loop them in (@lindsayolson) via a comment instead.

## <i class="fas fa-receipt" id="biz-tech-icons"></i> Team Budgets

We use [Allocadia](https://allocadia.com/) to manage team budgets. Due to a limited number of seats being allocated to the Marketing team, only `@dplanella` and `@nuritzi` have access to the tool from the Community Relations team (sign-in from Okta is required). They are responsible for coordinating with the Community Relations team to make sure that forecasts are properly added to Allocadia. 

As a workaround for easier collaboration, those with Allocadia access can do regular exports of the budget into a Google Spreadsheet by downloading the Community Relations budget by clicking on: `Export current view`. 

### How we create our annual budget
Like other teams at GitLab, the Community Relations team proposes an annual budget to cover the costs necessary for each of its subprograms. This total annual budget is then proposed to Marketing leadership, and to the e-group, for approval.

Once approved, the Community Relations team then forecasts how much of the total annual budget will be spent each quarter, and if possible, which month each spend will occur. The total annual budget cannot be adjusted except for in special circumstances with approval of the e-group.

### Reallocation and adjustments
Adjustments between the quarterly forecasts (moving budget from one quarter to another), can occur up until the specific quarter's forecasts are locked in. This occurs on the last day of the previous quarter (eg. the FY22 Q2 forecast is due on April 30th).

Once a quarter's forecast has been locked, no further adjustment to that quarter's spending is allowed. For example, if the forecasted amount for that quarter is not used up, then that amount is "lost" and cannot be reallocated to another quarter. 

Line-item adjustments can occur once the quarterly forecast is locked. To do so, a request needs to be submitted and approved by `@dplanella` or `@nuritzi` for the Community Relations team. 

### Managing Expenses and Actual Spend
Each month, the Community Relations team program managers meet with our parters in Finance to review actual monthly expenses during the monthly `Community Relations Accruals` meeting. This process allows us to accurately report our team expenditures vs budget forecasts. 

Costs over $5000 are allocated out month by month for a specified amount of time by the appropriate Community Program Manager. 

Costs under $5000 do not need to be allocated over time, and will be removed from the budget all at once.  

### Tracking swag giveaway expenses

To help our finance team keep track of community swag giveaways, giveaways in Printfection that support Community Relations programs and events should add `Community:` as a prefix to the campaign title. Ex: `Community: GitLab Heroes + Superheroes Swag`. 

#### Community Event Sponsorship
This section details how we make decisions about sponsoring community events.

Events Decision Tree:

```mermaid
graph TD
A(Event)
A --> A'{Account<br />specific?}
A' --> |YES| A''[Send to <br /> Field Marketing]
A' --> |NO| B{Size >1000?}
B -->|YES| C{Audience:<br /> jobseekers?}
B -->|NO| D{Audience: <br /> developers?}
C -->|YES| E[Send to<br /> People Ops]
C -->|NO| F[Send to<br /> Corporate Marketing]
D -->|YES| G{Fit for diversity<br />sponsorhip?}
D -->|NO| H{Regional event?}
G -->|YES| I["Send to<br />sponsorships@"]
G -->|NO| J[Send to<br />Community Relations]
H -->|YES| K{Thought leaders, <br />customers, <br />decision makers?}
H -->|NO| L[TBD]
K -->|YES| M[Send to<br /> Field Marketing]
K -->|YES| N{Hiring<br />event?}
N -->|YES| O[Ping People Ops]
N -->|NO| P["Consider sending to<br />events@"]
```

##### How to submit a community event sponsorship request

To submit a community event for support or sponsorship:

1. Review our events decision tree to ensure you are directing your event inquiry to the appropriate team.
1. Submit an issue using the [sponsorship-request](https://gitlab.com/gitlab-com/marketing/community-relations/community-operations/community-sponsorship-requests/issues/new?issuable_template=sponsorship-request) template.
1. For Service Desk or other auto-generated issues that contain sponsorship requests, we will retroactively apply the 'sponsorship-request' to the issue. The process for updating an issue with no template to the 'sponsorship-request' template is: copy text from original issue, assign 'sponsorship-request' template to issue, paste text from original issue into the appropriate field at bottom of template, update remaining fields.
1. GitLab's Open Source Program Manager will review the request and follow up with the event contact.

##### How we assess requests

We ask the following questions when assessing an event:

- Will there be a GitLab speaker? We do not require a speaker slot in return for sponsorship but we do prioritize events where the audience will be hearing about GitLab - either from a GitLab team member or a member of the wider GitLab community.
- What type of people will be attending the event? We prioritize events attended by diverse groups of developers with an interest in DevSecOps, Cloud Native, Kubernetes, Serverless, Multi-cloud, CI/CD, Open Source, and other related topics.
- Will we be able to interact with attendees? We prioritize events that provide opportunities for meetings, workshops, booth or stands to help people find us, and other interaction with attendees.
- Where will the event be held? We aim to have a presence at events around the globe with a particular focus on areas with large GitLab communities and large populations of developers.
- Is the event important for industry and/or open source visibility? We prioritize events that influence trends and attract leaders within the developer and open source communities. We also prioritize events organized by our strategic partners.
- What is the size of the opportunity for the event? We prioritize events based on audience size, the number of interactions we have with attendees, and potential for future contributions to GitLab from attendees.

Each question is graded on a scale of 0-2. We then tally the scores and assign the event to a sponsorship tier.

- Events scoring below 5 are not eligible for sponsorship.
- Events scoring 6-8 are eligible for GitLab swag for attendees and other in-kind sponsorship.
- Events scoring 9 or above are eligible for financial support.

We ask these questions and use this scorecard to ensure that we're prioritizing GitLab's and our community's best interests when we sponsor events.

If you have questions, you can always reach us by sending an e-mail to `community@gitlab.com`.


## Community Operations Response Channels

| Channel Name | Source      | Action.     |
|--------------|-------------|-------------|
| HackerNews    | Hacker News mentions via Zapier   | Find an expert, and collaborate with the Developer Evangelism Team in Slack |
| Hacker News front page stories    | Hacker News mentions via Zapier   | Find an expert, and collaborate with the Developer Evangelism Team in Slack |
| GitLab Forum    | Discourse (forum.gitlab.com)   | Find an expert in the Forum Contributors Group, and collaborate in Slack |
| GitLab Blog    | Disqus (blog.gitlab.com)   | Find an expert (usually the blog post author), and collaborate in Slack |

## Relevant Processes

It is not uncommon that the Social Media Team, and Communications Team at GitLab reaches out to Community Operations. Because of Community Operaions' response channels, and nature of the program, Community Operations has a unique view of the community's sentiment and tone. This means that the Community Operations Manager uses the following resources and handbook pages as needed, when partnering with Social and Comms to ensure a quality response back to our community in times of crisis or gerneral low sentiment and tone.

[Check out the social, community operations, and expert shared Community Management practices](/handbook/marketing/corporate-marketing/social-marketing/community-management/)

### Social Media Team

[Social Media Guidelines](https://about.gitlab.com/handbook/marketing/social-media-guidelines/)

### Expert Responses

Occasionally the Community Operations Manager will encourage GitLab Team Members, as experts, to engage with the wider GitLab community following our Team Member [Social Media Guidelines](/handbook/marketing/social-media-guidelines/) as well as these additional flexible guidelines, listed here:  

1. Experts are encouraged be themselves, while using the [GitLab Writing Style Guidelines](/handbook/communication/#writing-style-guidelines) - we always want to be personable and human in our online presence.
2. Engage as yourself whenever possible. (ex. use your personal Twitter handle)
3. Always try and link to relevant documentation, issues, handbook pages, forum topics and other resources.

#### Examples of when to involve an expert

- Technical questions or comments on a GitLab blog post
- HackerNews questions about a strategic GitLab topic (e.g. CI, security) or misinformation about GitLab
- Forum topics re: specific use cases not covered in the docs or relevant product feedback


#### Slack Template for Involving Experts

> @expert_username [LINK TO COMMUNITY COMMENT] Hello! An expert is needed to respond to this. Could you please answer on [name of social platform] using your own individual account?  If you don't know the answer, could you share your thoughts and ping a specific expert who might? Or if there is a more appropriate channel to ask, could you point me in that direction? Thanks!

### Forum Uses

#### Announcements

In the past the Product Management Team has sucessfully used the forum as a space to foster conversation and encourage feedback on changes to the GitLab product. [Here is an example of when we changed our subscription model.](https://forum.gitlab.com/t/new-gitlab-product-subscription-model/45923)

#### Announcement Components

#### Banner (optional)

Steps for bannering on Discourse
1. Download design component to your computer
2. In Discourse navigate to Admin Panel > Customize > Themes > Top Navbar
3. Under uploads, click the `Add+` button
4. Add your design 
5. Click `Edit CSS/HTML`
6. Add mini banner CSS:
   ```
   .mini-banner__image {
     height: 60.5px;
     width: 720px;
     background-image: url($mini-banner);
     background-size: 100% 100%;
     margin: 0 auto;
   }
   ```
7. Click `After Header and add this div to make CSS work`
   ```
   <div class="main-banner">
     <a href="https://forum.gitlab.com">
       <div class="main-banner__image"></div>
     </a>

     <a href="https://forum.gitlab.com/t/ci-cd-minutes-for-free-tier/40241" target="__blank">
       <div class="mini-banner__image"></div>
     </a>
   </div>
   ```
8. Slight modifications may be needed depending on the size of the image

#### Announcement Draft Post

Typically it is the task of the Product Manager or DRI to draft and wordsmith the announcement post. This will happen via an issue, so the forum DRI can simpy copy and paste the language into the forum draft verbatim. 

In order to visualize and prepare for the upcoming announcement do this in the [Staff category](https://forum.gitlab.com/c/staff/4) That way users won't be able to see the announcement before it's time, and the forum DRI will be able to add links to the Announcement's FAQ and/ or Blog post, as well as link to [GitLab's Code of Conduct](https://about.gitlab.com/community/contribute/code-of-conduct/). 

Steps for creating a draft post

1. Navigate to the [Staff category](https://forum.gitlab.com/c/staff/4)
2. Click `+New Topic`
3. Paste in content/language from issue
4. Add approved suject line (this will end up being the URL)
5. Click `Create topic`
6. Share screenshot of topic draft in related issed since most Product Managers do not have permissions to visit the Staff category. 

When it's time for the Draft post to "go live"

1. Click the big pencil next to the subject line
2. Choose appropriate category from the dropdown underneath the subject line

Optional steps are: 
1. Change the topic owner from yourself to the Product DRI by clicking on the wrench icon on the right of the screen and select from dropdown
2. Change the timestamp by clicking on the wrench icon on the right of the screen and select from dropdown

### Support for Education, Open Source and Startup Programs

Community Operations supports the [Education](/handbook/marketing/community-relations/education-program), [Open Source](/handbook/marketing/community-relations/opensource-program/) and [Startups](/solutions/startups/) Programs, and their Program Managers.

The Community Operations team helps process and manage program applications as per the [community programs applications workflow](/handbook/marketing/community-relations/community-operations/community-program-applications).

#### Common Community Program metrics

Due to the similarity among our Community Programs' workflows and goals, the Community Operations team helps keep track of [common community program metrics](/handbook/marketing/community-relations/community-operations/workflows/metrics/). 
