---
layout: markdown_page
title: "GitOps GTM Sales Play: Infrastructure Automation"
---

## Overview

### What is GitOps?

GitOps is an operational framework that takes DevOps best practices used for application development such as version control, collaboration, compliance, and CI/CD, and applies them to infrastructure automation. More details [here](https://about.gitlab.com/handbook/marketing/strategic-marketing/usecase-gtm/gitops/#what-is-gitops)

### Sales Play Objective

Expand DevOps best practices to Infrastructure teams

### Who is this sales play for?  

* Primary: SDRs who call on new leads generated via associated campaign
* Primary: SALs and AEs who call on one or more existing GitLab customers  
* Secondary: SAs and TAMs who support one or more existing GitLab customers  

## Getting Started

### Who to meet

- *Economic Buyer*: - Director/VP/CIO of IT, Head of IT Infrastructure / Platform Engineering
- *User*: SRE, Infra Engineer, Sys Admin, Platform Engineer - have to execute frequent repetitive tasks to support dynamically changing elastic environments

More details [here](https://about.gitlab.com/handbook/marketing/strategic-marketing/usecase-gtm/gitops/#personas)

### Discovery questions

- How does your infrastructure team manage configurations, policies, variables etc consistently across your infrastructure?
- Are you able to ensure that changes are reviewed and approved by the right individuals - to ensure minimal disruption to your staging / production environments?
- Is your infrastructure team able to consistently repeat the steps to setup an environment every single time?

More questions [here](https://about.gitlab.com/handbook/marketing/strategic-marketing/usecase-gtm/gitops/#discovery-questions)

### Value discovery

### Common Pains

| Challenges "before scenarios" | So What? "negative consequences" |
| ----------------------------- | -------------------------------- |
|  |  |

### Common benefits

| Desired Future State (“After Scenarios”) | So What? (“Positive Business Outcomes”) |
| ----------------------------- | -------------------------------- |
|  |  |

### Required capabilities

| Required capability	 | Customer Metrics |
| ----------------------------- | -------------------------------- |
|  |  |

## Positioning value

### Elevator pitch

Have you faced infrastructure downtime and have not been able to trace it back to who made the changes, what changes were made and who approved it? Infrastructure automation with GitLab helps you bring Application DevOps best practices of collaboration, version control, CI/CD & Compliance to Infrastructure.

### Value Proposition (How GitLab does it?)

Unlike other vendors, GitOps with GitLab helps you manage physical, virtual and cloud native infrastructures. We use a tight integration with industry-leading infrastructure automation tools like Terraform, AWS Cloud Formation, and the like to meet you where you are.

Detailed section on How GitLab meets the market requirements [here](https://about.gitlab.com/handbook/marketing/strategic-marketing/usecase-gtm/gitops/#market-requirements)

### Differentiators (How GitLab does it better?)

- Most competitors support predominantly cloud native - GitLab meets customers where they are supporting On-premise & cloud, physical, virtual, cloud native infrastructures
- Most competitive solutions require 5-6 integrations to achieve the same. GitLab offers version control, CI/CD and container registry & out of the box integration for config management, orchestration & infra provisioning.
- Agent-based and Agentless approach - giving customers a choice to pick the right approach for their environments

More details [here](https://about.gitlab.com/handbook/marketing/strategic-marketing/usecase-gtm/gitops/#top-3-differentiators)

### Competitors
Primary competitors: Flux (Weaveworks), Argo CD
Secondary competitors: Codefresh, Transposit, Red Hat/IBM

Detailed competitive against primary competitors [here](https://about.gitlab.com/handbook/marketing/strategic-marketing/usecase-gtm/gitops/#competitive-comparison)

### Handling Objections

Typical questions:
- Our environment is too complex for GitOps
- GitOps gives more access to developers to fiddle with deployments, and infra teams are not comfortable with that
- (Infra / DevOps Engineer) I'm going to lose control over my work and environment
- I don't use Kubernetes, so GitOps is not relevant for me

Detailed list of Q&A [here](https://about.gitlab.com/handbook/marketing/strategic-marketing/usecase-gtm/gitops/#objection-handling)

### Customer Stories

- [WAG Labs](https://about.gitlab.com/blog/2019/01/16/wag-labs-blog-post/)
- [North Western Mutual](https://www.youtube.com/watch?v=yw7N82mXmZU)
- [Kiwi.com](https://www.youtube.com/watch?v=Un2mJrRFSm4)
- [VMware](https://www.youtube.com/watch?v=qXj4ShQZ4IM)
- [ValidaTek](https://www.youtube.com/watch?v=3uZE-ktP2Pc)
- [Gartner Peer Insights](https://about.gitlab.com/handbook/marketing/strategic-marketing/usecase-gtm/gitops/#gartner-peer-insights)

### Resources

- Pathfactory link to share with customers - https://gitlab.lookbookhq.com/authoring/content-library/target/39413
- Complete list of presentations, web pages, whitepapers, blogs and videos available [here](https://about.gitlab.com/handbook/marketing/strategic-marketing/usecase-gtm/gitops/#resources).

## Sales Play Tactics

### SALs and AEs
TBD

### SDR
TBD


## Related Marketing Campaign

TBD
