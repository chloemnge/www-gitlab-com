---
layout: markdown_page
title: "FY22 DevOps Platform Sales Play"
---

## Jump to [Sales Play Tactics](#sales-play-tactics)

## Overview

**Objective** - . 

Who is this sales play for?  
* Primary: SDRs, SALs approaching new prospects 
* Secondary: SALS, AEs, TAMs calling on existing customers and looking to broaden adoption by new roles

## Who to meet

**Ideal Customer Profiles**  
- Transformation-minded leaders
- Management personas interested in greater efficiency

- Bonus points for:
   - Heavy focus on cross-team collaboration
   - Multiple roles being filled by individual contributors (e.g., needing to jump from development to security or product manager to tester)
   - Multiple-vendor DevOps toolchain in place
   - Existing resources already assigned to integration and maintenance

**Target Buyer Personas**

| Persona role  | Possible titles|
| ------------- |:----------------------:| 
| Economic buyers    | Head of Product, CTO, CIO, CISO, VP / Director of App Development |
| Technical influencers    | Chief Architect, App Dev Manager, DevOps Engineer | 
| Other Personas to consider | PMO, Release & Change Management |


**Target Account Lists** 

- [Account list](TBD)

## Getting Started

Consider the following questions: 
* Does the organization have any strategic transformation initiatives that could benefit from a single platform for collaboration?
* Is there existing interdepartmental friction brought about by differently aligned priorities? 
* Is the customer already managing multiple tools in a toolchain?
* Is the customer early in their agile journey and seeking guidance on the best path forward?
* Are you engaged with the right personas/teams (see Target Buyer Personas above)?
* Do you have access to power/authority (a business decision maker)?
* Who are your champions within the account?
 
## Value Discovery


### Common Pains  

An in-depth view of pain points and probing questions around them can be found on the [DevOps Platform resource page](https://about.gitlab.com/handbook/marketing/strategic-marketing/usecase-gtm/devops-platform/#current-state-wheres-the-pain)


### Common Benefits  

By adopting GitLab's DevOps Platform, customers may experience one or more of the below benefits:

| Desired Future State (“After Scenarios”) | So What? (“Positive Business Outcomes”)   |
| ------------- |:-------------:| 
| TBD   | TBD |
| TBD | TBD |
| TBD   | TBD |
| TBD | TBD |
| TBD   | TBD |
| TBD | TBD |


### Required Capabilities  

To achieve the positive business outcomes highlighted above, what required capabilities does the customer need to solve for and how will success be measured?

| Required capability |  Customer Metrics  |
| ------------- |:-------------:| 
| TBD   | TBD |
| TBD | TBD |
| TBD   | TBD |
| TBD | TBD |
| TBD   | TBD |
| TBD | TBD |

## Engaging the Customer

To better understand your customer's needs, choose from [this list of discovery questions](https://about.gitlab.com/handbook/marketing/strategic-marketing/usecase-gtm/devops-platform/).


## Positioning Value 

### Elevator pitches 

Below are one-sentence summaries of value you can deliver to user and buyer personas to position relevant aspects of the DevOps Platform use case's value, based on the market segment of the persona's company.

{::options parse_block_html="true" /}

<div class="panel panel-info">

**SMB**
{: .panel-heading}

<div class="panel-body">

|   User: [Delaney - the Development Team Lead](/handbook/marketing/strategic-marketing/roles-personas/#delaney-development-team-lead)	|   User: [Devon - the DevOps Engineer](/handbook/marketing/strategic-marketing/roles-personas/#devon-devops-engineer)	|   Buyer: CTO<code>&ast;</code>	|  Buyer: CIO<code>&ast;</code> 	|
|---	|---	|---	|---	|
|   Receive immediate feedback on the quality, performance, and security of code as soon as you commit. Collaborate seamlessly among teams.	|   Grow without having to build and support custom integrations.	|   Collaborate in a single system, minimize context-switching, and increase developer productivity and focus.	|   Grow without building and supporting custom integrations. Scale and manage a single system.	|
{: .custom-class #custom-id}

*<code>&ast;</code> CTO (Dev, external focus) and CIO (Ops, internal focus) personas in development and may vary per segment.*

</div>

##### **Mid-Market**
{: .panel-heading}

<div class="panel-body">

|   User: [Delaney - the Development Team Lead](/handbook/marketing/strategic-marketing/roles-personas/#delaney-development-team-lead)	|   User: [Devon - the DevOps Engineer](/handbook/marketing/strategic-marketing/roles-personas/#devon-devops-engineer)	|   Buyers: range from [Erin - the Application Development Executive](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/buyer-persona/#erin---the-application-development-executive-vp-etc) to the CTO<code>&ast;</code>	|  Buyers: range from [Kennedy - the Infrastructure Engineering Director](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/buyer-persona/#kennedy---the-infrastructure-engineering-director) to the CIO<code>&ast;</code> 	|
|---	|---	|---	|---	|
|   Receive immediate feedback on the quality, performance, and security of code as soon as you commit. Collaborate seamlessly among teams.	|   Increase reliability and eliminate ad hoc, team-based integrations.	|  Collaborate in a single system, minimize context-switching and waiting, identify and remove productivity blockers, and deliver more value faster with more productive, focused developers. 	|   Increase reliability and performance while you grow by eliminating custom integrations. Scale and manage a single system.	|
{: .custom-class #custom-id}

*<code>&ast;</code> CTO (Dev, external focus) and CIO (Ops, internal focus) personas in development and may vary per segment.*

</div>

##### **Enterprise**
{: .panel-heading}

<div class="panel-body">

|   User: [Delaney - the Development Team Lead](/handbook/marketing/strategic-marketing/roles-personas/#delaney-development-team-lead)	|   User: [Devon - the DevOps Engineer](/handbook/marketing/strategic-marketing/roles-personas/#devon-devops-engineer)	|   Buyer: [Erin - the Application Development Executive](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/buyer-persona/#erin---the-application-development-executive-vp-etc)	|  Buyer: [Kennedy - the Infrastructure Engineering Director](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/buyer-persona/#kennedy---the-infrastructure-engineering-director) 	|
|---	|---	|---	|---	|
|   Receive immediate feedback on the quality, performance, and security of code as soon as you commit. Collaborate seamlessly among teams.	|   Increase reliability and eliminate ad hoc, team-based integrations.	|   Collaborate in a single system, minimize context-switching and waiting, identify and remove productivity blockers, and deliver more value faster with more productive, focused developers.	|   Increase reliability and performance while you grow by eliminating custom integrations. Scale and manage a single system.	|
{: .custom-class #custom-id}

</div></div>


### How GitLab Does It

- [How GitLab meets the market requirements for a DevOps Platform](https://about.gitlab.com/handbook/marketing/strategic-marketing/usecase-gtm/devops-platform/#how-gitlab-meets-the-market-requirements) 

### How GitLab Does It Better 

With GitLab, organizations TBD. 

**[Key GitLab differentiators](TBD)** include:

1. TBD
1. TBD
1. TBD
1. TBD
1. TBD
1. TBD
1. TBD


*see provided link for additional details including value and videos*

**[Proof points](https://about.gitlab.com/handbook/marketing/strategic-marketing/usecase-gtm/devsecops/#proof-points---customers)**


## Objection handling


**Most common objections**

| Objection  | Response    |
| ------------- |:-------------:| 
| "I don't want to rip and replace my toolchain." | There's no need to do a rip-and replace. GitLab can coexist with any number of tools for as long as you need. You can start small on a green field project and prove out the value of GitLab before adopting more widely. If you have a tool you just can't move for now, simply integrate it, use GitLab for the rest, and consider replacing the legacy tool in the futre if that's right for you.  |
| "We've spent a lot of time building/customizing/tuning _product x_."   | What if you could stop spending that effort on tool maintainence and spend it instead on delivering higher quality software faster? |
| "We have very specialized DevOps needs." | We've developer a true DevOps Platform. That means you can build on it easily if you need to, either through our [many integrations](https://docs.gitlab.com/ee/integration/), or by [contributing](https://docs.gitlab.com/ee/development/contributing/) to our [open source cor](https://about.gitlab.com/blog/2016/07/20/gitlab-is-open-core-github-is-closed-source/)e. You probably won't need to, however. [Our customers](https://about.gitlab.com/customers/) have very specific and demanding requirements, and we've been able to meet them. |
| "I'm not going to use all of it." | You don't need to use everything GitLab has to offer, but it will always be available to you,and since we're a true platform delivered as a single application, adding new features when you're ready is really just as easy as pushing a button. We have thousands of customers who started with just one or two features and adopted the entire solution because it was more cost effective and simpler to maintain. |



## Services

GitLab (or a GitLab partner) offers the below services to help accelerate time to value and mitigate risk:
* TBD
* TBD

## Additional Resources 

* TBD
* TBD
* TBD
* TBD
* TBD


# Sales Play Tactics {#sales-play-tactics}

## Related Campaign activies to drive leads to you

TBD

## Actions for sales to take

Note TBD: When sales gets the lead, will it show from which email it came? If so, sales will want to align the meeting purpose to the CTA from which the lead came. If sales will not see from which email the lead came, they can choose where in this flow is most appropriate to begin.

1. **Initial qualification meeting 1** - use #value-discovery above to assess business objectives. Use these recommended assets based upon areas of most interest:
   * TBD
**Milestones:** Identify key value driver, champion, and economic buyer, agree to second meeting  
**Metric:** Sales Accepted Opp

1. **Meeting 2** - purpose is to understand customer’s interest in one or more of the topics of interest and provide a deep dive on:
   * TBD

**Milestones:** Identify key value driver, champion, and economic buyer, agree to meeting with economic buyer  
**Metric:** Sales stage x

1. **Meeting 3** - purpose is to use the provided template and assess opportunity and show how TBD.
   * TBD
**Milestones:** agree to next meeting  
**Metric:** Sales stage x 


## Resources to use 

Will have specific resources under actions above. This is for additional resources.

* TBD
* TBD
* TBD

## How to measure progress

**Milestones** (identified by sales stages and/or SDLC fields)
- [ ] Gameplan with GitLab champion
- [ ] Meeting with economic buyer
- [ ] Agreement to do POV
- [ ] POV requirements defined
- [ ] Successful POV
- [ ] Proposal 


**Metrics:** 
- [ ] Avg days per stage to progress
- [ ] Longest step (common blockage?)
- [ ] Economic buyer titles - common factor?
- [ ] Retro on sales play


Note: progress of the GTM Motion will be measured at the campaign level with clicks/opens/page visits, SAO (is there a code sales needs to use in SFDC?)




