---
layout: handbook-page-toc
title: "GitLab Technical Certifications"
description: "Explore how GitLab Professional Services certifies engineers to validate their readiness to deliver Consulting Services offerings."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## GitLab Technical Certifications

GitLab offers technical certifications to help the GitLab community and team members validate their ability to apply GitLab in their daily DevOps work. To earn certification, candidates must first pass a written assessment, followed by a hands-on lab assessment graded by GitLab Professional Services engineers. 

### Overview

GitLab is planning and developing several technical certifications to help the GitLab community and team members validate their ability to apply GitLab in their daily DevOps work. To earn certification, candidates must first pass a written assessment, followed by a hands-on lab assessment graded by GitLab Professional Services engineers. 

#### Certification Journey Structure and Flow

##### Structure

There are three levels of GitLab Technical Certification.

1. Associate
2. Specialist
3. Professional

The Associate level is a prerequisite for the Specialist level, and the Specialist level is a prerequisite for the Professional level.

##### Flow

The following diagram shows the journey from Associate through Professional for the GitLab Certified DevOps Professional certification.

```mermaid
graph LR;
  
  A((GitLab Certified Associate))-->B([GitLab Certified CI/CD Specialist]);
  style A fill:#bbf,stroke:#380D75,stroke-width:2px
  style B fill:#bbf,stroke:#380D75,stroke-width:2px
  style C fill:#bbf,stroke:#380D75,stroke-width:2px
  style D fill:#bbf,stroke:#380D75,stroke-width:2px
  style E fill:#bbf,stroke:#380D75,stroke-width:2px
  A((GitLab Certified Associate))-->C([GitLab Certified Project Management Specialist]);
  A((GitLab Certified Associate))-->D([GitLab Certified Security Specialist]);
  E((GitLab Certified DevOps Professional))
  B --> E
  C--> E
  D--> E
```

### Why certification?

#### For employers
Team managers now have a way to confirm their team members possess the skills needed to effectively use GitLab in their daily DevOps tasks. This helps ensure the team will be able to successfully adopt GitLab and make the most of the organization's investment.

#### For individuals

Individual GitLab users who earn certification receive a certification logo they can share on social media to showcase their accomplishment. This helps highlight to colleagues and employers their proficiency in effectively using the GitLab platform.

### Currently available certifications

Here are the certifications GitLab has soft-launched in FY'21. Each set of certification assessments is currently available to [GitLab Professional Services](/handbook/customer-success/professional-services-engineering/) customers who purchase the related course. Course participants gain access to the certification assessments immediately after completing their course sessions. GitLab is planning to offer asynchronous options for anyone to prepare for and take the certification assessments, with online availability targeted for the end of calendar year 2020.

#### GitLab Certified Associate

- Soft-launched in: May 2020
- Description page: [GitLab Certified Associate](https://about.gitlab.com/services/education/gitlab-certified-associate/)
- Related course: [GitLab with Git Basics](https://about.gitlab.com/services/education/gitlab-basics/)
- Also available at: [GitLab Commit 2020](https://about.gitlab.com/events/commit/)

#### GitLab Certified CI/CD Specialist

- Soft-launched in: July 2020
- Description page: [GitLab Certified CI/CD Specialist](https://about.gitlab.com/services/education/gitlab-cicd-specialist/)
- Related course: [GitLab CI/CD Training](https://about.gitlab.com/services/education/gitlab-ci/)
- Also available at: [GitLab Commit 2020](https://about.gitlab.com/events/commit/)

#### GitLab Certified InnerSourcing Specialist

- Soft-launched in: June 2020
- Description page: [GitLab Certified InnerSourcing Specialist](https://about.gitlab.com/services/education/gitlab-innersourcing-specialist/)
- Related course: [GitLab InnerSourcing Training](https://about.gitlab.com/services/education/innersourcing-course/)

#### GitLab Certified Project Management Specialist

- Soft-launched in: December 2020
- Description page: [GitLab Certified Project Management Specialist](https://about.gitlab.com/services/education/gitlab-project-management-specialist/)
- Related course: [GitLab for Project Managers Training](https://about.gitlab.com/services/education/pm/)

#### GitLab Certified Security Specialist

- Soft-launched in: December 2020
- Description page: [GitLab Certified Security Specialist](https://about.gitlab.com/services/education/gitlab-security-specialist/)
- Related course: [GitLab Security Essentials Training](https://about.gitlab.com/services/education/security-essentials/)

#### GitLab Certified DevOps Professional

- Soft-launched in: December 2020
- Description page: [GitLab Certified DevOps Professional](https://about.gitlab.com/services/education/gitlab-certified-devops-pro/)
- Related course: [GitLab DevOps Fundamentals Training](https://about.gitlab.com/services/education/devops-fundamentals/)

