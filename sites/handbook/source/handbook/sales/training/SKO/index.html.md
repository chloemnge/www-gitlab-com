---
layout: handbook-page-toc
title: "Sales Kickoff"
description: "The GitLab Sales Kickoff sets the tone for the GitLab field organization for the new fiscal year"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# Sales Kickoff (SKO) Overview
This event sets the tone for the GitLab field organization for the new fiscal year with a focus on the following goals:
1.  **Motivate**: Be energized about GitLab’s vision & strategy and the incredible opportunities ahead (for each and every GitLab team member, the company, our customers & partners)
1.  **Celebrate**: Recognize and enjoy the successes of the prior fiscal year
1.  **Enable**: Ensure every field team member returns home with a clear understanding of what’s needed to plan and execute their business for the new fiscal year (based on functional enablement priorities from senior leadership) 

# Sales Kickoff 2021
SKO 2021 took place virtually during the week of Feb 8-11, 2021. Check out the SKO 2021 Peak Performance theme reveal and pre-event promo videos below.

<figure class="video_container">
  <iframe src="https://player.vimeo.com/video/487653102?color=fc6d26&title=0" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<figure class="video_container">
  <iframe src="https://player.vimeo.com/video/503035779?color=fc6d26" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

## High-level Sales Kickoff 2021 Agenda 
Virtual SKO 2021 will consist of half-day sessions each morning (PT) during the week of Feb 8-11, 2021. We will offer live content in two time zones to accommodate our team members around the world. Below is a high-level agenda for each time zone: 

### AMER/EMEA Time Zone
*  Pre work: Embrace remote → learn about product strategy/roadmap prior to SKO. 
*  Mon, 2021-02-08: Keynotes, Customer Speaker, Q&As, regional team building (AMER)
*  Tues, 2021-02-09: Keynotes, Q&As, regional team building (EMEA)
*  Wed, 2021-02-10: Awards ceremony, role-based breakout sessions, Partner Summit
*  Thurs, 2021-02-11: Role-based breakout sessions, Comp Plan information session 

### APAC Time Zone (Based on AEDT)
*  Pre work: Embrace remote → learn about product strategy/roadmap prior to SKO.
*  Tues, 2021-02-09: Keynotes, Customer Speaker, Q&As, regional team building
*  Wed, 2021-02-10: Keynotes, Q&As, regional team building (EMEA)
*  Thurs, 2021-02-11: Awards ceremony, role-based breakout sessions, Partner Summit
*  Fri, 2021-02-12: Role-based breakout sessions, Comp Plan information session 

** Note that agenda details are subject to change. 

## SKO 2021 Tickets 
You must book a ticket for this event. This is how we keep track of who is attending and all other details. Please look out for registration details via email and Slack in January 2021. Registration should be completed no later than 2021-02-05. 

We will  validate all registrations on the back end, and any unapproved registrants will be canceled. Please do not book if you are not approved to attend this event. See the FAQs below for more information about who is approved to attend SKO. 

**Ticket Types:**
- GitLab Team Member Attendees - GitLab Internal Team Members (Sales, Customer Success, Channel, Alliances, SDR and supporting teams) 
- Channel Partner Ticket - External channel partners joining GitLab Sales Kickoff

## Sales Kickoff 2021 FAQ 
**Q: What are the dates and location?**

A: Sales Kickoff (SKO) will take place virtually from Feb 8-11, 2021. 

**Q: Why are we having a virtual event versus an in-person SKO?**

A: The health and safety of our team is our top priority and a remote event allows us to keep planning with confidence while there is still uncertaintly about when and how to host large in-person events.

**Q: Will we still have an in-person Sales event in 2021?** 

A: We hope to! Sales leadership is looking into opportunities to get the team together in person either by region or with the entire team in 2H FY22. This is contingent on the status of the COVID-19 pandemic, and we will make all decisions with the health and safety of our team members as our top priority.

**Q: What platform(s) are we using for the virtual event?**

A: We are using Hopin as our virtual event platform. This is the same platform we used for Contribute. 

**Q: When and how should I register?**

A: The SKO core team has reached out via email with registration links. Please complete registration through this email by 2021-02-05.

**Q: Who is approved to attend SKO?**

A: SKO attendees are set and approved by Sales and Marketing leadership. Attendees include all Field Team members (Sales, Customer Success, Channel, Alliances, SDR), select Marketing attendees appointed by the CMO, and other supporting team members from People Ops, Legal and Product. 

If you believe you should be included in SKO based on the categories outlined above, please reach out to David Somers (for Field and other supporting teams) or Todd Barr/Marketing leadership (for Marketing team members).

**Q: How are we accommodating different time zones?**

A: We will have live and interactive sessions for all time zones. All sessions will be recorded and available for team members on-demand after the event.

**Q: Which time zone should I attend?**

A: You will be added to the registration for the time zone that coordinates with the country where you are based – AMER/EMEA or APAC. If you'd like to view the Save the Date invites for the alternate time zone, you can do so on the [Field Enablement Calendar](https://calendar.google.com/calendar/u/0?cid=Z2l0bGFiLmNvbV81bjNnNjBsNTh0aHVtOWFvdnA4aWlzYXYzNEBncm91cC5jYWxlbmRhci5nb29nbGUuY29t) (navigate to the week of February 8, 2021).

**Q: Will there be any pre-work?**

A: In support of GitLab’s all-remote culture, all SKO participants are  asked to complete pre-work to help you get the most out of the planned content and make the best use of our time together.

To access the course, go to gitlab.edcast.com, sign in via SSO, click on "My Learning Plan," then click on the "SKO 2021 Pre-Work" course. The pre-work will take approximately 2-2.5 hours to complete, so please plan accordingly.

**Q: What will I be expected to participate in?**

A: SKO is a great opportunity for our team to come together to both celebrate our results and collaborate for a strong start in the new year. As such, the agenda is purposely crafted with numerous activities to learn, network and celebrate together. We expect that every SKO to treat it as a “work trip” and block your calendar to ensure you fully participate in every part of the event.

**Q: Is there a dress code for this virtual event?**

A: While the event is virtual, we ask that you please regard the dress code like you would for any virtual customer or external meeting. You will be networking and interacting with your peers and Channel partners. We encourage you to be as engaged as possible – show up ready to ask questions, take notes and meet new people! 

**Q: I just got hired – how do I attend SKO?**

A: All eligible SKO attendees have until 2021-02-05 to register for the event. If you are a new hire and you have not received a registration email, please Slack #sko-virtual-2021. 

**Q: How will we make SKO 2021 exciting even though it's virtual?**

A: The SKO core team is preparing an engaging virtual event. Everything from the virtual platform to the agenda to the event theme is being carefully curated to ensure that our team feels connected and energized while participating in SKO. 

**Q: Will we still have an Awards Dinner and networking time?**

A: Rather than an Awards Dinner, we will host an Awards Ceremony on Wednesday. The SKO core team is planning fun and creative ways to ensure 2021 Sales Awards winners are celebrated. We have also built networking time into the agenda each day to give team members time to connect across the organization and with partners.

**Q: How will we include partners in SKO 2021?**

A: We will host a Partner Summit in tandem with SKO. Partners will be invited to participate in our Day 1 keynotes on Tuesday, and they will have their own Partner Summit-specific sessions on Wednesday. Partners will be available to virtually meet and greet with GitLab team members in the virtual Partner Booths each day of SKO after the half-day keynotes or breakout sessions have concluded.

**Q: Where can I ask questions related to SKO?**

A: If your question is not covered in the FAQ, please feel free to post in the [feedback issue](https://gitlab.com/gitlab-com/sales-team/field-operations/enablement/-/issues/535). Alternatively, you can visit the public slack channel #sko-virtual-2021 and ask your question there. If your question is not-public, you can email sales-events at gitlab dot com with questions. Members of the core SKO planning team, including Emily Kyle, David Somers and Monica Jacob are actively monitoring this email and will respond to your inquiry within 24 hours. 

We are actively planning this event and will update this FAQ as more information becomes available. 

Reference: [Company Code of Conduct](https://about.gitlab.com/community/contribute/code-of-conduct/)

## SKO 2021 Day 1 Sessions

| **Session Name** | **Presentation** | **Video** |
| ------ | ------ | ------ |
| Welcome to GitLab Sales Kickoff 2021 | [slides](https://docs.google.com/presentation/d/1tYA4skru1M8n41D41ZIA-4klgEiq3BRwRxXo2FF-tHg/edit?usp=sharing) (GitLab internal) | [video](https://gitlab.edcast.com/insights/keynote-welcome) (GitLab internal) |
| Product Keynote: A Winning Product for Peak Performance | [slides](https://docs.google.com/presentation/d/1NunKRuZBnDcZi0k_N5-NQBvdy5X5ZaV4a1Qf7w126qs/edit?usp=sharing) (GitLab internal) | [video](https://gitlab.edcast.com/insights/keynote-product) (GitLab internal) |
| Customer Spotlight: H-E-B and GitLab: A Happy Marriage | [slides](https://docs.google.com/presentation/d/1-PZn0IlB6SYvPMDW2zcF06mqAY61u9b-Dnv0vxRgJTI/edit?usp=sharing) (GitLab internal) | [video](https://gitlab.edcast.com/insights/customer-spotlight) (GitLab internal) |
| Customer Spotlight: Trendyol & GitLab: How International Expansion Drives Business Needs | [slides](https://docs.google.com/presentation/d/1Hk0sJnqtNwmuakQ3_awOuY_lJJkbzeOD1zVn5bG8Trw/edit?usp=sharing) (GitLab internal) | [video](https://gitlab.edcast.com/insights/keynote-trendyol) (GitLab internal) |

## SKO 2021 Day 2 Sessions

| **Session Name** | **Presentation** | **Video** |
| ------ | ------ | ------ |
| Marketing Keynote: Prepare for Peak Performance | [slides](https://docs.google.com/presentation/d/175Qyf6TGGN8vgDaCrBKek6hOGMA-LHYfqjl9or-RPyE/edit?usp=sharing) (GitLab internal) | [video](https://gitlab.edcast.com/insights/keynote-marketing) (GitLab internal) |
| Channel Keynote: Cover More Ground with Channel Partners | [slides](https://docs.google.com/presentation/d/18OL3wiTPvhK4ZEzhHcxIErE5WqtmGpdJdMyr0fFqHDM/edit#slide=id.gb5ab6e1814_0_0) (GitLab internal) | [video](https://gitlab.edcast.com/insights/keynote-channel) (GitLab internal) |
| Alliances Keynote: Winning is a Team Sport! | [slides](https://docs.google.com/presentation/d/1DOzBi5Jrc7kUxhaECy13hR1LBv6TRvx2JP7JlYtvEN8/edit?usp=sharing) (GitLab internal) | [video](https://gitlab.edcast.com/insights/keynote-alliances) (GitLab internal) |
| Closing Keynote: It's Time for Peak Performance | [slides](https://docs.google.com/presentation/d/1qjQIuynuPQg6rGuNQSzKFSgdS8q_QJLzlmYujRcP3VE/edit) (GitLab internal) | [video](https://gitlab.edcast.com/insights/keynote-closing) (GitLab internal) |

## SKO Awards Ceremony

| **Session Name** | **Presentation** | **Video** |
| ------ | ------ | ------ |
| SKO FY21 Awards Ceremony | [slides](https://docs.google.com/presentation/d/1ur1lNCIqLutvsZQdaP8Gt8lQ-OWsEB-Lr2Vvxt99oxc/edit?usp=sharing) (GitLab internal) | [video](https://gitlab.edcast.com/insights/special) (GitLab internal) |
| FY21 SKO - President's Club | [slides](https://docs.google.com/presentation/d/11jEBujHeSDjGPvhmjYIhaMt7BzyaJXknfN47o3_nuEI/edit?usp=sharing) (GitLab internal) | [video](https://gitlab.edcast.com/insights/special) (GitLab internal) |

## SKO 2021 Role-Based Breakout Sessions

### Enterprise Sales Strategic Account Leaders

| **Session Name** | **Presentation** | **Video** |
| ------ | ------ | ------ |
| Landing New Logos | [slides](https://docs.google.com/presentation/d/1LlbAppVrITBviI5_fM_JeEhDh7eUfnV50LhXiuu1xzI/edit?usp=sharing) (GitLab internal) | [video](https://gitlab.edcast.com/insights/breakout-landing) (GitLab internal) |
| Expand Strategy Into Security and Plan | [slides](https://docs.google.com/presentation/d/1oq7ODy9TJpuZqH_tvVtCm2t-C0QkTbuG4ZRlRzRNcUY/edit?usp=sharing) (GitLab internal) | [video](https://gitlab.edcast.com/insights/breakout-expand) (GitLab internal) |
| Account Planning: Master the Climb | [slides](https://docs.google.com/presentation/d/1JPWozZ83z1kStjpo7d0gQV4sq5MLGWKg-ZpXA2qlfiw/edit?usp=sharing) (GitLab internal) | [video](https://gitlab.edcast.com/insights/breakout-account-planning) (GitLab internal) |
| Pivoting the Pricing Conversation | [slides](https://docs.google.com/presentation/d/12VxCqUkLAOMrCA5Af3MYwiUyy-nkxbr33bwa9IJfFG8/edit?usp=sharing) (GitLab internal) | [video](https://gitlab.edcast.com/insights/breakout-pivoting) (GitLab internal) |

### Commercial Sales Account Executives

| **Session Name** | **Presentation** | **Video** |
| ------ | ------ | ------ |
| Co-Selling with Services Partners | [slides](https://docs.google.com/presentation/d/13YgoPyLryyP8Mnxq8xALZvH6e01ltbSlKi8htMU2gmY/edit?usp=sharing) (GitLab internal) | [video](https://gitlab.edcast.com/insights/breakout-co-selling) (GitLab internal) |
| Pitching to the Economic Buyer | [slides](https://docs.google.com/presentation/d/1XY04MGHsS17LnT_KdqF62op6hK1v34flb9roAvq9miE/edit?usp=sharing) (GitLab internal) | [video](https://gitlab.edcast.com/insights/breakout) (GitLab internal) |
| Placing Customers in the Right Tier | [slides](https://docs.google.com/presentation/d/1FrJ9Xgh83l1lYn3jXOYM3aGbdd1XautBHlXoXd1MoCU/edit?usp=sharing) (GitLab internal) | [video](https://gitlab.edcast.com/insights/placing) (GitLab internal) |
| Finding the "Why Now" | [slides](https://docs.google.com/presentation/d/1oli6HEGT7lStuEZ4MD5tL_ndl_TKEQveVHX5RRscTrs/edit?usp=sharing) (GitLab internal) | [video](https://gitlab.edcast.com/insights/breakout-finding) (GitLab internal) |

### Channel Sales Account Managers

| **Session Name** | **Presentation** | **Video** |
| ------ | ------ | ------ |
| Co-Selling with Services Partners | [slides](https://docs.google.com/presentation/d/13YgoPyLryyP8Mnxq8xALZvH6e01ltbSlKi8htMU2gmY/edit?usp=sharing) (GitLab internal) | [video](https://gitlab.edcast.com/insights/breakout-gitlab) (GitLab internal) |
| Planning for Joint GitLab and Partner Success | [slides](https://docs.google.com/presentation/d/1Bw-kIgYlIfX4kyOS4Wgb_k0PjXJ3u4-WdD0b4xxWhtU/edit?usp=sharing) (GitLab internal) | [video](https://gitlab.edcast.com/insights/breakout-planning) (GitLab internal) |
| Growing Our Partner Business (Individual and CSM Partner Portfolio) | [slides](https://docs.google.com/presentation/d/126nNQ5VX-kFNVxfI1YBQL9uWo9EVh3P7tmabQwAX2UI/edit?usp=sharing) (GitLab internal) | [video](https://gitlab.edcast.com/insights/breakout-growing) (GitLab internal) |
| Building Pipeline with Your Partners - MDF and Marketing Campaigns | [slides](https://docs.google.com/presentation/d/1dsMBEgzH5La3zEO81cuNtmyJJJKBDPThJt7WU2fcJNY/edit?usp=sharing) (GitLab internal) | [video](https://gitlab.edcast.com/insights/breakout-building) (GitLab internal) |

### Solution Architects

| **Session Name** | **Presentation** | **Video** |
| ------ | ------ | ------ |
| Landing New Logos | [slides](https://docs.google.com/presentation/d/1LlbAppVrITBviI5_fM_JeEhDh7eUfnV50LhXiuu1xzI/edit?usp=sharing) (GitLab internal) | [video](https://gitlab.edcast.com/insights/breakout-landing) (GitLab internal) |
| Expand Strategy Into Security and Plan | [slides](https://docs.google.com/presentation/d/1oq7ODy9TJpuZqH_tvVtCm2t-C0QkTbuG4ZRlRzRNcUY/edit?usp=sharing) (GitLab internal) | [video](https://gitlab.edcast.com/insights/breakout-expand) (GitLab internal) |
| Strategies to Win Without a Long PoV (Proof of Value) | [slides](https://docs.google.com/presentation/d/1TfrWlH6lhGbbBd0y6LOL5SYYvoqCgGz0fW1o_GBMn-U/edit?usp=sharing) (GitLab internal) | [video](https://gitlab.edcast.com/insights/breakout-strategies) (GitLab internal) |
| Leveling Up the Conversation with the 5 Whys | [slides](https://docs.google.com/presentation/d/1-oTZjDykh3tInL9sU4xzSpn9JarWO_mMGYOVTpmHmcE/edit?usp=sharing) (GitLab internal) | [video](https://gitlab.edcast.com/insights/breakout-leveling) (GitLab internal) |

### Technical Account Managers

| **Session Name** | **Presentation** | **Video** |
| ------ | ------ | ------ |
| Account Based Strategy & Marketing for Customers | [slides](https://docs.google.com/presentation/d/1Ea1Fa7q7sXa_O552xlrJwHWf8YXFS7YCTIErjEJxMo0/edit?usp=sharing) (GitLab internal) | [video](https://gitlab.edcast.com/insights/breakout-account) (GitLab internal) |
| Expand Strategy Into Security and Plan | [slides](https://docs.google.com/presentation/d/1oq7ODy9TJpuZqH_tvVtCm2t-C0QkTbuG4ZRlRzRNcUY/edit?usp=sharing) (GitLab internal) | [video](https://gitlab.edcast.com/insights/breakout-expand) (GitLab internal) |
| Embracing Differences with Social Styles | [slides](https://docs.google.com/presentation/d/1c8HQHPzNLIK68LA7QSEEFuekeNEB45VlTCzQP_qDN2c/edit?usp=sharing) (GitLab internal) | no video |
| Leveling Up the Conversation with the 5 Whys | [slides](https://docs.google.com/presentation/d/1-oTZjDykh3tInL9sU4xzSpn9JarWO_mMGYOVTpmHmcE/edit?usp=sharing) (GitLab internal) | [video](https://gitlab.edcast.com/insights/breakout-leveling) (GitLab internal) |

### Professional Services Engineers

| **Session Name** | **Presentation** | **Video** |
| ------ | ------ | ------ |
| How to Work with Partners: Tactically and Technically | [slides](https://docs.google.com/presentation/d/1k0MjqEnu6-zp7SyeZGddRfCeEx_iG3CYIg2l237hlc0/edit?usp=sharing) (GitLab internal) | no video |
| Professional Services Methodology: Step by Step | [slides](https://docs.google.com/presentation/d/1XK_gALJgA13E-vUN-lv9fg5d1BmQ7ciG6nlkIK2BAKM/edit?usp=sharing) (GitLab internal) | no video |
| Using Congregate to Migrate Customers to GitLab | [slides](https://docs.google.com/presentation/d/1YL_A4uhrtPfUfPNzrJRq6stDFhMw-k4-jKOH-65LLaY/edit?usp=sharing) (GitLab internal) | no video |
| Leveling Up the Conversation with the 5 Whys | [slides](https://docs.google.com/presentation/d/1-oTZjDykh3tInL9sU4xzSpn9JarWO_mMGYOVTpmHmcE/edit?usp=sharing) (GitLab internal) | [video](https://gitlab.edcast.com/insights/breakout-leveling) (GitLab internal) |

### Sales Development Reps

| **Session Name** | **Presentation** | **Video** |
| ------ | ------ | ------ |
| Thriving as a Remote SDR: From Cold-calling Jiu-jitsu to Planning Long Term Career Success (featuring Tito Bohrt) | no slides | [video](https://gitlab.edcast.com/insights/thriving) (GitLab internal) |
| Converting Free to Paid | [slides](https://docs.google.com/presentation/d/1VtKYJXowqh8amuW6opg2HzAJfhoOD_9PEEIVoAzF6CU/edit?usp=sharing) (GitLab internal) | [video](https://gitlab.edcast.com/insights/breakout-converting) (GitLab internal) |
| SDR Competitive Blueprint | [slides](https://docs.google.com/presentation/d/1RPkiF99Ogq7xr9S87tG5pQKYOHQVnlWok31beeTdHlY/edit?usp=sharing) (GitLab internal) | no video |
| Security: Our Biggest Differentiator | [slides](https://docs.google.com/presentation/d/1Rog--UoCV71SvbdMdgkpMwY_s29tb8B5R2vR77J5jAQ/edit?usp=sharing) (GitLab internal) | [video](https://gitlab.edcast.com/insights/security-as) (GitLab internal) |

### Field Marketing

| **Session Name** | **Presentation** | **Video** |
| ------ | ------ | ------ |
| Account Based Strategy & Marketing for Customers | [slides](https://docs.google.com/presentation/d/1Ea1Fa7q7sXa_O552xlrJwHWf8YXFS7YCTIErjEJxMo0/edit?usp=sharing) (GitLab internal) | [video](https://gitlab.edcast.com/insights/breakout-account) (GitLab internal) |
| Field Marketing Goals for FY22 | [slides](https://docs.google.com/presentation/d/1u1EOXR8WJsf464ILjMoun8gne2gazU9pGfHHjensp2c/edit?usp=sharing) (GitLab internal) | no video |
| Building Pipeline with Your Partners - MDF and Marketing Campaigns | [slides](https://docs.google.com/presentation/d/1dsMBEgzH5La3zEO81cuNtmyJJJKBDPThJt7WU2fcJNY/edit?usp=sharing) (GitLab internal) | [video](https://gitlab.edcast.com/insights/breakout-building) (GitLab internal) |

----

## [Sales Kickoff 2020](/handbook/sales/training/SKO/2020)

